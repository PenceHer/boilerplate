﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Boilerplate.Tests
{
    [TestClass()]
    public class WaveStackTests
    {
        [TestMethod()]
        public void MinimumHoldTest()
        {
            var priceHistory = new int[] { 100, 102, 99, 100, 105, 100 };
            Assert.AreEqual(-1, WaveStack.MinimumHold(priceHistory, -1));
            Assert.AreEqual(1, WaveStack.MinimumHold(priceHistory, 0));
            Assert.AreEqual(3, WaveStack.MinimumHold(priceHistory, 1));
            Assert.AreEqual(1, WaveStack.MinimumHold(priceHistory, 2));
            Assert.AreEqual(1, WaveStack.MinimumHold(priceHistory, 3));
            Assert.AreEqual(-1, WaveStack.MinimumHold(priceHistory, 4));
            Assert.AreEqual(-1, WaveStack.MinimumHold(priceHistory, 5));
            Assert.AreEqual(-1, WaveStack.MinimumHold(priceHistory, 10));
        }

        [TestMethod()]
        public void MinimumHoldTimesTest()
        {
            var priceHistory = new int[] { 100, 102, 109, 99, 98, 97 };
            var expectedHoldTimes = new int[] { 1, 1, -1, -1, -1, -1 };
            var actualHoldTimes = WaveStack.MinimumHoldTimes(priceHistory);
            CompareArrays(expectedHoldTimes, actualHoldTimes);

            priceHistory = new int[] { 100, 102, 99, 100, 105, 100 };
            expectedHoldTimes = new int[] { 1, 3, 1, 1, -1, -1 };
            actualHoldTimes = WaveStack.MinimumHoldTimes(priceHistory);
            CompareArrays(expectedHoldTimes, actualHoldTimes);

            priceHistory = new int[] { 105, 102, 99, 100, 105, 110 };
            expectedHoldTimes = new int[] { 5, 3, 1, 1, 1, -1 };
            actualHoldTimes = WaveStack.MinimumHoldTimes(priceHistory);
            CompareArrays(expectedHoldTimes, actualHoldTimes);
        }

        [TestMethod()]
        public void NextGreaterElementsTest()
        {
            var input = new int[] { 1, 2, 1 };
            var expected = new int[] { 2, -1, 2 };
            var output = WaveStack.NextGreaterElements(input);
            CompareArrays(expected, output);
        }

        [TestMethod()]
        public void NextGreaterElementTest()
        {
            var nums1 = new int[] { 4, 1, 2 };
            var nums2 = new int[] { 1, 3, 4, 2 };
            var expected = new int[] { -1, 3, -1 };
            var output = WaveStack.NextGreaterElement(nums1, nums2);
            CompareArrays(expected, output);
        }

        private void CompareArrays(int[] expected, int[] actual)
        {
            Compare.CompareArrays(expected, actual);
        }
    }
}