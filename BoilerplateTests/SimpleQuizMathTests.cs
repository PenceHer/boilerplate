﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Boilerplate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boilerplate.Tests
{
    [TestClass()]
    public class SimpleQuizMathTests
    {
        [TestMethod()]
        public void ReservoirSamplingTest()
        {
            int value = 5;
            var reservoir = new List<int> { 1, value, 3, 11, 7, value, 8, value, 3, 10 };
            var results = new int[10];
            int samples = 1000;

            for (int i = 0; i < samples; i++)
            {
                int index = SimpleQuizMath.ReservoirSampling(value, reservoir);
                results[index]++;
            }

            double expected = samples / 3.0;
            double tolerance = 0.05 * samples;

            Assert.AreEqual(0, results[0]);
            Assert.AreEqual(expected, results[1], tolerance);
            Assert.AreEqual(0, results[2]);
            Assert.AreEqual(0, results[3]);
            Assert.AreEqual(0, results[4]);
            Assert.AreEqual(expected, results[5], tolerance);
            Assert.AreEqual(0, results[6]);
            Assert.AreEqual(expected, results[7], tolerance);
            Assert.AreEqual(0, results[8]);
            Assert.AreEqual(0, results[9]);
        }

        [TestMethod()]
        public void ProbabilityTest()
        {
            int trues = 0;

            for (int i = 0; i < 100; i++)
            {
                if (SimpleQuizMath.Probability(0.5))
                    trues++;
            }

            Assert.AreEqual(50, trues, 10);
        }

        [TestMethod()]
        public void ChangeReferenceTest()
        {
            int value = 5;
            SimpleQuizMath.ChangeReference(ref value, 10);
            Assert.AreEqual(10, value);
        }
    }
}