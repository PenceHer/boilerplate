﻿using Boilerplate;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Boilerplate.Tests
{
    [TestClass()]
    public class BinaryTreeTests
    {
        [TestMethod()]
        public void RecursiveInorderTraversalTest()
        {
            var root = new TreeNode(1)
            {
                right = new TreeNode(2)
                {
                    left = new TreeNode(3)
                }
            };
            var output = new BinaryTree().RecursiveInorderTraversal(root);
            var expected = new int[] { 1, 3, 2 };
            Compare.CompareArrays(expected, output.ToArray());
        }

        [TestMethod()]
        public void InorderTraversalTest()
        {
            var root = new TreeNode(1)
            {
                right = new TreeNode(2)
                {
                    left = new TreeNode(3)
                }
            };
            var output = new BinaryTree().InorderTraversal(root);
            var expected = new int[] { 1, 3, 2 };
            Compare.CompareArrays(expected, output.ToArray());
        }

        [TestMethod()]
        public void PostorderTraversalTest()
        {
            var root = new TreeNode(1)
            {
                right = new TreeNode(2)
                {
                    left = new TreeNode(3)
                }
            };
            var output = new BinaryTree().PostorderTraversal(root);
            var expected = new int[] { 3, 2, 1 };
            Compare.CompareArrays(expected, output.ToArray());
        }
    }
}