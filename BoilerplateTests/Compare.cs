﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Boilerplate.Tests
{
    public static class Compare
    {
        public static void CompareArrays<T>(T[] expected, T[] actual)
        {
            if (expected.Length != actual.Length)
            {
                Assert.Fail();
            }

            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
        }
    }
}
