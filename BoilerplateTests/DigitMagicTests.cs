﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Boilerplate.Tests
{
    [TestClass()]
    public class DigitMagicTests
    {
        [TestMethod()]
        [DataRow(12, 21)]
        [DataRow(21, -1)]
        [DataRow(230241, 230412)]
        [DataRow(1999999999, -1)]
        public void NextGreaterElementTest(int n, int expected)
        {
            int nextGreater = new DigitMagic().NextGreaterElement(n);
            Assert.AreEqual(expected, nextGreater);
        }
    }
}