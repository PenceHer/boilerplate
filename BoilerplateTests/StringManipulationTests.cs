﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Boilerplate.Tests
{
    [TestClass()]
    public class StringManipulationTests
    {
        [TestMethod()]
        [DataRow("Hello World!", "!dlroW olleH")]
        [DataRow("   ", "   ")]
        [DataRow(" test ", " tset ")]
        public void ReverseStringTest(string input, string output)
        {
            string result = StringManipulation.ReverseString(input);
            Assert.AreEqual(output, result);
        }
    }
}