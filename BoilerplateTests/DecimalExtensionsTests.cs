﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Boilerplate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boilerplate.Tests
{
    [TestClass()]
    public class DecimalExtensionsTests
    {
        [TestMethod()]
        public void ChangeDecimalPlacesTest()
        {
            decimal one = 1.23M;
            decimal two = 1.0000M;
            decimal three = 4.56789M;
            decimal four = one * two;
            decimal five = two * three;
            decimal six = two * one;
            decimal seven = three * two;
        }
    }
}