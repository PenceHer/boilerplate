﻿using System.Collections.Generic;

namespace Boilerplate
{
    public class DigitMagic
    {
        public int NextGreaterElement(int n)
        {
            var digits = GetDigits(n);
            digits = SwapDigits(digits);
            long nextGreater = GetNumber(digits);

            return nextGreater > int.MaxValue ? -1 : (int)nextGreater;
        }

        private static List<int> SwapDigits(List<int> digits)
        {
            int smallest = -1;

            for (int i = digits.Count - 2; i >= 0; i--)
            {
                for (int j = i + 1; j < digits.Count; j++)
                {
                    if (digits[j] > digits[i] && (smallest < 0 || digits[j] < digits[smallest]))
                    {
                        smallest = j;
                    }
                }

                if (smallest >= 0)
                {
                    int swap = digits[smallest];
                    digits[smallest] = digits[i];
                    digits[i] = swap;
                    SortAfterSwap(digits, i + 1);
                    return digits;
                }
            }

            return new List<int>() { -1 };
        }

        private static void SortAfterSwap(List<int> digits, int sortStart)
        {
            var subDigits = digits.GetRange(sortStart, digits.Count - sortStart);
            subDigits.Sort();
            for (int i = sortStart; i < digits.Count; i++)
            {
                digits[i] = subDigits[i - sortStart];
            }
        }

        private static long GetNumber(List<int> digits)
        {
            digits.Reverse();
            long number = 0;
            long order = 1;

            foreach (int digit in digits)
            {
                number += digit * order;
                order *= 10;
            }

            return number;
        }

        private static List<int> GetDigits(int n)
        {
            var digits = new List<int>();
            long order = 1;

            while (n >= order)
            {
                int nextDigit = (int)(n % (order * 10));
                digits.Add((int)(nextDigit / order));
                n -= nextDigit;
                order *= 10;
            }

            digits.Reverse();
            return digits;
        }
    }
}
