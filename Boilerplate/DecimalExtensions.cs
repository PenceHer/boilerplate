﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boilerplate
{
    public static class DecimalExtensions
    {
        public static decimal ChangeDecimalPlaces(this decimal value, byte decimalPlaces)
        {
            try
            {
                double doubleValue = (double)value * Math.Pow(10, decimalPlaces);
                if (doubleValue > int.MaxValue || doubleValue < int.MinValue)
                {
                    return value;
                }
                int percent = (int)doubleValue;
                return new decimal(percent, 0, 0, false, decimalPlaces);
            }
            catch
            {
                return value;
            }
        }
    }
}
