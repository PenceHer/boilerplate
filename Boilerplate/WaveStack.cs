﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Boilerplate
{
    public class WaveStack
    {
        public static int[] NextGreaterElement(int[] nums1, int[] nums2)
        {
            var nextGreater = new int[nums1.Length];
            for (int i = 0; i < nums1.Length; i++)
            {
                nextGreater[i] = NextGreater(nums2, nums1[i]);
            }
            return nextGreater;
        }

        private static int NextGreater(int[] nums2, int num)
        {
            int indexOfNum = IndexOf(nums2, num);
            for (int i = indexOfNum + 1; i < nums2.Length; i++)
            {
                if (nums2[i] > num)
                {
                    return nums2[i];
                }
            }

            return -1;
        }

        private static int IndexOf(int[] nums, int num)
        {
            for (int i = 0; i < nums.Length; i++)
            {
                if (nums[i] == num)
                {
                    return i;
                }
            }

            throw new Exception();
        }

        public static int[] NextGreaterElements(int[] nums)
        {
            var nextGreatest = Enumerable.Repeat(-1, nums.Length).ToArray();
            var unmatchedElements = new Stack<int>();

            for (int i = 0; i < nums.Length; i++)
            {
                while (unmatchedElements.Count > 0 && nums[i] > nums[unmatchedElements.Peek()])
                {
                    nextGreatest[unmatchedElements.Pop()] = nums[i];
                }

                unmatchedElements.Push(i);
            }

            for (int i = 0; i < nums.Length - 1; i++)
            {
                while (unmatchedElements.Count > 0 && nums[i] > nums[unmatchedElements.Peek()])
                {
                    nextGreatest[unmatchedElements.Pop()] = nums[i];
                }
            }

            return nextGreatest;
        }

        //You are given an integer array which shows the value of a stock for n days. 
        //Write a program which returns the minimum number of days the stock must be held to make profit, if it was bought on day 'i'. If it is not possible to make a profit then return -1.
        //[100, 102, 109, 99, 98, 97] returns [1, 1, -1, -1, -1, -1]
        //[100, 102, 99, 100, 105, 100] returns [1, 3, 1, 1, -1, -1]
        public static int[] MinimumHoldTimes(int[] priceHistory)
        {
            var holdTimes = Enumerable.Repeat(-1, priceHistory.Length).ToArray();
            var unmatchedDays = new Stack<int>();

            for (int i = 0; i < priceHistory.Length; i++)
            {
                while (unmatchedDays.Any() && priceHistory[unmatchedDays.Peek()] < priceHistory[i])
                {
                    int day = unmatchedDays.Pop();
                    holdTimes[day] = i - day;
                }

                unmatchedDays.Push(i);
            }

            return holdTimes;
        }

        public static int MinimumHold(int[] priceHistory, int startDay)
        {
            if (startDay < 0 || startDay >= priceHistory.Length)
            {
                return -1;
            }

            int remainingDays = priceHistory.Length - 1 - startDay;
            int startingPrice = priceHistory[startDay];

            for (int i = startDay + 1; i <= startDay + remainingDays; i++)
            {
                if (priceHistory[i] > startingPrice)
                {
                    return i - startDay;
                }
            }

            return -1;
        }
    }
}
