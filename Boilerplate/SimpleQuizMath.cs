﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boilerplate
{
    public static class SimpleQuizMath
    {
        private static Random _random;
        private static Random Random => _random ?? (_random = new Random());

        public static int ReservoirSampling(int value, List<int> reservoir)
        {
            int selectedIndex = -1;
            int samplesFound = 0;

            for (int i = 0; i < reservoir.Count; i++)
            {
                if (reservoir[i] == value && Probability(1.0 / ++samplesFound))
                {
                    selectedIndex = i;
                }
            }

            return selectedIndex;
        }

        public static bool Probability(double chance)
        {
            return Random.NextDouble() < chance;
        }

        public static void ChangeReference<I>(ref I value, I newValue)
        {
            value = newValue;
        }
    }
}
