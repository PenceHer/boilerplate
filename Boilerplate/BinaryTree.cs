﻿using System.Collections.Generic;

namespace Boilerplate
{
    public class BinaryTree
    {
        public IList<int> PostorderTraversal(TreeNode root)
        {
            var visited = new List<int>();
            var passed = new Stack<TreeNode>();
            var node = root;
            TreeNode lastNodeVisited = null;

            while(true)
            {
                if (node != null)
                {
                    passed.Push(node);
                    node = node.left;
                }
                else if (passed.Count > 0)
                {
                    var next = passed.Peek();

                    if (next.right != null && lastNodeVisited != next.right)
                    {
                        node = next.right;
                    }
                    else
                    {
                        visited.Add(next.val);
                        lastNodeVisited = passed.Pop();
                        node = null;
                    }
                }
                else { break; }
            }

            return visited;
        }

        public IList<int> InorderTraversal(TreeNode root)
        {
            var visited = new List<int>();
            var passed = new Stack<TreeNode>();

            var node = root;
            while(true)
            {
                if (node != null)
                {
                    passed.Push(node);
                    node = node.left;
                }
                else if (passed.Count > 0)
                {
                    node = passed.Pop();
                    visited.Add(node.val);
                    node = node.right;
                }
                else { break; }
            }

            return visited;
        }

        public IList<int> RecursiveInorderTraversal(TreeNode root)
        {
            var visitedNodes = new List<int>();
            Traverse(root, visitedNodes);
            return visitedNodes;
        }

        private void Traverse(TreeNode node, List<int> visitedNodes)
        {
            if (node == null) return;

            Traverse(node.left, visitedNodes);
            visitedNodes.Add(node.val);
            Traverse(node.right, visitedNodes);
        }
    }

    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int x) { val = x; }
    }
}
