﻿using System.Linq;

namespace Boilerplate
{
    public static class StringManipulation
    {
        public static string ReverseString(string input)
        {
            return new string(input.Reverse().ToArray());
        }
    }
}
